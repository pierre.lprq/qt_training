# coding: utf8
"""View module."""

import os
from PySide2 import QtCore
from PySide2.QtQuick import QQuickView


class View(QQuickView):
    """View object."""

    def __init__(self, app_core):
        """View init function."""
        super(View, self).__init__()

        # Core
        self.core = app_core

        # Link with UI
        self.engine = self.engine()
        self.context = self.engine.rootContext()
        self.context.setContextProperty("context", self)

        # Load the QML file
        project_path = os.path.dirname(os.path.abspath(__file__))
        self.qml_file = os.path.join(project_path, "qml", "main.qml")
        self.setSource(QtCore.QUrl.fromLocalFile(self.qml_file))

        # Watcher
        qml_path = os.path.join(project_path, "qml")
        ui_files = [os.path.join(qml_path, x) for x in os.listdir(qml_path)]
        self.watcher = QtCore.QFileSystemWatcher(ui_files)
        self.watcher.fileChanged.connect(self.update_ui)

        # Optionnal
        self.setMinimumSize(QtCore.QSize(400, 300))

    def update_ui(self, path):
        self.engine.clearComponentCache()
        self.setSource(QtCore.QUrl.fromLocalFile(self.qml_file))

    @QtCore.Slot(QtCore.QObject)
    def delete_widget(self, widget):
        widget.deleteLater()

