import QtQuick 2.6
 
Item {
    id: root
    property string text: "ceci est un test"
    Rectangle {
        id: background
        anchors.centerIn: root
        color: "#ccccff"
        radius: parent.height/2
        width: root.width
        height: root.text.length ? root.height + 2 : 0
        border.color: "#bbbbff"
        border.width: 1
        smooth: true
    }
    Text {
        id : tagText
        anchors.centerIn: root
        text: root.text
    }
}