import QtQuick 2.6
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Item {
    id: root

    enum Status {Incorrect, Correct, Neutral}

    // Default
    property string txt_label: "Write your text here"
    property string txt_log: "Log"
    property int enum_field_status: CustomTextField.Status.Correct

    // Completion model - list of dict : [{"label": name, "output": output}]
    property string completion_key_item_label: "label"
    property string completion_key_item_output: completion_key_item_label //"output"
    property int completion_item_height: 26
    property int completion_max_item: 8

    // Functions
    property var fct_is_valid: function(text) {
        // Return the field status
        //  - 0 / false / CustomTextField.Status.Incorrect : Incorrect
        //  - 1 / true / CustomTextField.Status.Correct : Correct
        //  - 2 / CustomTextField.Status.Neutral : Neutral
        return true
    }
    property var fct_text_update: function(text) {
        Qt.createQmlObject('import QtQuick 2.0; Rectangle {color: "red"; width: 20; height: 20}', parentItem, "dynamicSnippet1");
    }
    property var fct_get_text: function() { return text_field.text }
    property var fct_set_text: function(text) { text_field.text = text }
    property var fct_set_log: function(text) { root.txt_log = text }
    property var fct_set_model: function(model) { completion_list.model = model }
    property var fct_on_item_clicked: function(modelData) { console.log(modelData[completion_key_item_output]) }

    // Customization
    property int text_size: 0
    property color clr_text: "white"
    property color clr_subtext: "silver"
    property color clr_hover: "#1A1A1A"
    property color clr_selection: "steelblue"
    property color clr_field_selected: "#161616"
    property color clr_field_not_selected: "#3B3B3B"
    property color clr_border: "grey"
    property color clr_status_correct: "#00B000"
    property color clr_status_incorrect: "orangered"
    property color clr_status_neutral: "steelblue"
    property color clr_completion_item_1 : "#262626"
    property color clr_completion_item_2 : "#202020"

    // Private
    property var _get_color: function() {
        if (enum_field_status == CustomTextField.Status.Correct) {return clr_status_correct}
        if (enum_field_status == CustomTextField.Status.Incorrect) {return clr_status_incorrect}
        if (enum_field_status == CustomTextField.Status.Neutral) {return clr_status_neutral}
        return "whitesmoke"
    }

    height: 62
    width: 200

    ColumnLayout {
        id: column_layout
        anchors.fill: parent
        anchors.topMargin: little_label.implicitHeight * 0.55
        spacing: 1

        TextField {
            id: text_field
            Layout.fillHeight: true
            Layout.fillWidth: true

            selectByMouse:true
            font.pixelSize: 15 + root.text_size
            color: root.clr_text
            selectionColor: root.clr_selection
            placeholderText: qsTr(root.txt_label)
            validator: RegExpValidator { regExp: /[a-zA-Z0-9_, -]*/ }

            onAccepted: {
                text_field.focus = false
                root.enum_field_status = root.fct_is_valid(text_field.text)
            }

            onTextChanged: {
                root.fct_text_update(text_field.text)
            }

            background: Rectangle {
                radius: 3
                color: text_field.activeFocus? root.clr_field_selected : root.clr_field_not_selected
                border.color: root.clr_border
                border.width: text_field.activeFocus? 1 : 0
            }

            // Field Status
            Rectangle {
                height: text_field.activeFocus? 2 : 1
                width: text_field.width

                anchors.bottom: text_field.bottom

                color: root._get_color()
            }

            // Little Label
            Rectangle {
                id: little_label
                implicitHeight: little_label_text.implicitHeight
                implicitWidth: little_label_text.implicitWidth + 10

                x: 10
                y: - little_label_text.implicitHeight * 0.45
                z: 1

                color: root.clr_field_not_selected
                radius: 3
                visible: text_field.text ? true : false

                Text {
                    id: little_label_text
                    anchors.centerIn: parent
                    text: root.txt_label
                    font.pixelSize: 11 + root.text_size
                    color: root.clr_subtext
                }
            }

            // Auto-Completion
            Popup {
                id: auto_completion
                height: {
                    var nbr_item = 0
                    if (completion_list.model.length < root.completion_max_item)
                        nbr_item = completion_list.model.length
                    else
                        nbr_item = root.completion_max_item
                    return nbr_item * (root.completion_item_height + completion_list.spacing) - completion_list.spacing + 2
                }
                width: text_field.width
                y: text_field.height

                visible: {
                    if (text_field.activeFocus && completion_list.model.length != 0)
                        return true
                    return false
                }

                background: Rectangle {
                    radius: 1
                    color: clr_border
                }

                closePolicy: Popup.NoAutoClose

                Component {
                    id: completion_item_container

                    Rectangle {
                        id: completion_item
                        property color item_color: index % 2 ? clr_completion_item_1 : clr_completion_item_2

                        height: root.completion_item_height
                        width: completion_list.width
                        radius: 1
                        color: completion_item.item_color

                        Text {
                            anchors.fill: parent

                            verticalAlignment: Text.AlignVCenter
                            padding: 10
                            text: modelData[root.completion_key_item_label]
                            font.pixelSize: 13 + root.text_size
                            color: root.clr_text
                        }

                        MouseArea {
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                root.fct_on_item_clicked(modelData)
                            }

                            onEntered: {
                                completion_item.color = root.clr_hover
                            }

                            onExited: {
                                completion_item.color = completion_item.item_color
                            }
                        }
                    }
                }

                ListView {
                    id: completion_list
                    anchors.centerIn: parent
                    width: auto_completion.width - 2
                    height: auto_completion.height - 2

                    model: []
                    delegate: completion_item_container
                    spacing: 1
                    clip: true
                }
            }
        }

        // LOG
        Rectangle {
            id: log
            implicitHeight: log_text.implicitHeight
            Layout.fillWidth: true
            color: "transparent"

            Text {
                id: log_text
                anchors.fill: parent
                anchors.leftMargin: 10

                text: root.txt_log
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 13 + root.text_size
                color: root._get_color()
            }
        }
    }
}