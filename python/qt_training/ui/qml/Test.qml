import QtQuick 2.6
 
Rectangle {
    id: root
    width: 360
    height: 360
    property QtObject tag: Qt.createQmlObject("Tag{}", root, "error")
 
    TextEdit {
        id: edit
        focus: true
        onTextChanged: {
            var c = text[text.length-1];
            if (c == ' ' || c == ';' || c == '.' || c == ',') {
                text = text.substring(0, text.length-1);
                tag.x = edit.x
                tag.text = edit.text
                tag.width = edit.width + edit.font.pixelSize/2
                x = x + tag.width + 10
                tag.height = edit.font.pixelSize + 2
                tag = Qt.createQmlObject("Tag{}", root, "error")
                text = ""
            }
        }
    }
}