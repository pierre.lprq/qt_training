import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Rectangle {
    id: root
    //property var fct: function() { Qt.createQmlObject("Widget{}", ctnr, "error") }
    //property QtObject widget: Rectangle{anchors.fill: parent; color: "green"}

    anchors.fill: parent
    color: "black"
    border.color: "whitesmoke"
    border.width: 2

    ColumnLayout{
        id: c_layout

        anchors.fill: parent
        anchors.margins: 20

        Item{
            id: ctnr
            property QtObject widget
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

        Button{
            id: button_1
            Layout.preferredHeight: 20
            Layout.fillWidth: true

            onClicked: {
                if (ctnr.widget) {
                    context.delete_widget(ctnr.widget)
                } 
                else {
                    ctnr.widget = Qt.createQmlObject("Widget{}", ctnr, "dynamic item")
                }
            }
        }

        Button{
            id: button_2
            Layout.preferredHeight: 20
            Layout.fillWidth: true

            onClicked: {
                if (ctnr.widget) {
                    context.delete_widget(ctnr.widget)
                } 
                else {
                    ctnr.widget = Qt.createQmlObject("Tag{}", ctnr, "dynamic item")
                }
            }
        }
    }

    /*Rectangle {
        id: framework
        anchors.centerIn: parent
        height: custom.height
        width: custom.width

        color: "transparent"
        border.color: "red"
        border.width: 1

        /*CustomButton{
            id: custom
        }

        /*CustomTextField3 {
            id: custom
            completion_key_item_output: "label"
            fct_text_update: function(text) {
                // Set Status Color
                enum_field_status = CustomTextField.Status.Neutral
                // Define Model
                var input_model = [{"label": "label-a"}, {"label": "label-b"}, {"label": "etiquette-c"}, {"label": "etiquette-d"}]
                var completion_model = []
                for (var i = 0; i < input_model.length; i++) {
                    if (text.length != 0 && input_model[i][completion_key_item_label].indexOf(text) >= 0) {
                        completion_model.push(input_model[i])
                    }
                }
                fct_set_model(completion_model)
            }
        }

        /*Text {
            id: custom
            anchors.centerIn: parent
            text: "Hello World!"
            color: "white"
        }
    }*/
}