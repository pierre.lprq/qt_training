import QtQuick 2.6
import QtQuick.Controls 2.1

Control  {
    id: root

    property int btn_duration: 100
    property int btn_radius: 20
    property bool btn_pressed: false

    z: 0

    height: 100
    width: 190

    background: Item {
        id: btn_bg

        Rectangle {
            id: btn_surf
            z: root.z + 1
            height: ctnr_text.implicitHeight
            width: root.width
            color: "green"
            radius: root.btn_radius

            Text{
                id: ctnr_text
                anchors.fill: parent
                text: "Bouton"
                font.pointSize: 40
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
            // On Press
            NumberAnimation on y {
                id: anim_press
                from: 0
                to: root.height
                duration: btn_duration
                running: false
            }
            // On Release
            NumberAnimation on y {
                id: anim_release
                from: root.height
                to: 0
                duration: btn_duration
                running: false
            }
        }
        Rectangle {
            z: root.z
            anchors.fill: parent
            anchors.top: btn_surf.verticalCenter
            color: "red"
            radius: root.btn_radius
        }
    }

    MouseArea {
        anchors.fill: parent
        //btn_animation.running=true

        onEntered: {
            console.log("entered")
            root.btn_pressed=true
            anim_press.running=true
        }

        onPressed: {
            console.log("pressed")
        }

        onReleased: {
            console.log("released")
        }

        onClicked: {
            console.log("clicked")
        }

        onDoubleClicked: {
            console.log("double_clicked")
        }

        onExited: {
            console.log("exited")
            root.btn_pressed=false
            anim_release.running=true
        }
    }
}