import QtQuick 2.6
 
Rectangle {
    id: root
    property var fct: function() { console.log("widget") }

    anchors.fill: parent
    color: "blue"

    MouseArea{
        anchors.fill: parent

        onClicked: {
            root.fct()
        }
    }
}