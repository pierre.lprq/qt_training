# coding: utf8
"""Launcher module."""

import os
import sys

from PySide2.QtCore import QUrl
from PySide2.QtQuick import QQuickView
from PySide2.QtGui import QGuiApplication


def launcher(argv):
    app = QGuiApplication(argv)
    # Define a view
    view = QQuickView()
    # Define qml file to use to setup the view
    project_path = os.path.dirname(os.path.abspath(__file__))
    qml_file_path = os.path.join(project_path, "ui", "qml", "main.qml")
    view.setSource(QUrl.fromLocalFile(qml_file_path))
    # Display
    view.show()
    return sys.exit(app.exec_())

if __name__ == "__main__":
    launcher(argv=sys.argv)