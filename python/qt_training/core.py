# coding: utf8
"""Core module."""

class Core(object):
    """Core object."""

    def __init__(self):
        """Core init function."""
        super(Core, self).__init__()
