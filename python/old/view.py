# -*- coding: utf-8 -*-
__author__ = "Thomas Perilhou"

import os, sys, importlib, datetime, inspect, json
from PySide2 import QtCore, QtGui, QtWidgets
from ui import sanity_check_main_ui as main_ui_py_file
from ui import header, module

reload(main_ui_py_file)
reload(header)
reload(module)


class MyHeaderItem(QtWidgets.QTreeWidgetItem, header.Ui_header_frame):
    """Crée une instance de QTreeWidgetItem (item-titre/"header") par dossier de modules de vérification d'un même type.

       Attributs :
           mainFrame (obj) : Instance de QFrame destinée (i) à être insérée à l'objet courant et (ii) à contenir une 
                             instance de QLabel et une instance de QPushButton.
           name (str) : Nom du dossier comprenant les modules de vérification d'un même type.
           isChecked (bool) : Retourne "True" si l'instance de QPushButton intégrée à l'objet courant est enfoncée. 
                              Retourne "False" sinon.
           qtIsChecked (obj) : Enum spécifiant si le statut de l'objet QtCore.Qt est coché ou décoché. 
    """
    def __init__(self, parent=None, name=None):
        """Paramètres:
               parent (obj) : Instance de QTreeWidget à laquelle intégrer (en tant qu'item) l'objet en cours.
               name (str) : Intitulé de l'objet en cours issu du nom du dossier scanné correspondant.
                    
           Retours :
               None.
        """
        QtWidgets.QTreeWidgetItem.__init__(self, parent)
        self.mainFrame = QtWidgets.QFrame()
        self.setupUi(self.mainFrame)

        self.name = name.upper()

        self.set_header()
        

    def set_header(self):
        """Fixe les caractéristiques principales de l'item-titre ("header").

           Retours :
               None.
        """
        self.treeWidget().setItemWidget(self, 0, self.mainFrame)
        self.lab_sanityCheck_header.setText(self.name)
        self.setToolTip(0,"Checkers Family: %s" % self.name) 

        self.puB_sanityCheck_header.clicked.connect(self.reflect_check_status)


    def reflect_check_status(self):
        """Transmet le statut "pressé" ou non du QPushButton de l'item-titre ("header") aux QCheckBox des items enfants.

           Retours :
               None.
        """
        self.isChecked = self.puB_sanityCheck_header.isChecked()
        self.qtIsChecked = QtCore.Qt.Unchecked
        
        if self.isChecked:
            self.qtIsChecked = QtCore.Qt.Checked

        for index in xrange(self.childCount()):
            itemChild = self.child(index)
            if isinstance(itemChild, MyHeaderItem):
                itemChild.puB_sanityCheck_header.click()

            elif isinstance(itemChild, MyModuleItem):
                itemChild.chB_sanityCheck_module.setCheckState(self.qtIsChecked)


    def apply_sunken_frame(self):
        """Applique l'effet "enfoncé" ("sunken") à l'instance de QFrame intégrée à l'item-titre ("header") courant.

           Retours :
               None.
        """
        self.mainFrame.setFrameShadow(QtWidgets.QFrame.Sunken)


    def apply_raised_frame(self):
        """Applique l'effet "surélevé" ("raised") à l'instance de QFrame intégrée à l'item-titre ("header") courant.

           Retours :
               None.
        """
        self.mainFrame.setFrameShadow(QtWidgets.QFrame.Raised)        



class MyModuleItem(QtWidgets.QTreeWidgetItem, module.Ui_module_frame):
    """Crée une instance de QTreeWidgetItem (item-module) correspondant à un module de vérification.

       Attributs :
           mainFrame (obj) : Instance de QFrame destiné (i) à être inséré dans le QTreeWidgetItem courant et (ii) à 
                             contenir un QCheckBox.
           moduleItem (obj) : Module de vérification (fichier .py).
           logArea (obj) : Instance de QTextEditWidget destiné à afficher le journal lors de l'exécution de fonctions.
           standardFuncNamesList (list(str)) : Liste des noms des fonctions standards.
           greenColor (obj) : Instance de QColor correspondant à la couleur verte.
           orangeColor (obj) : Instance de QColor correspondant à la couleur orange.
           redColor (obj) : Instance de QColor correspondant à la couleur rouge.
           standardGreyColor (obj) : Instance de QColor correspondant à la couleur grise utilisée comme couleur de 
                                     police par défaut.                                    
           titleContentFont (obj) : Instance de QFont correspondant au formatage des titres du journal.
           logNormalFont (obj) : Instance de QFont correspondant au formatage du contenu du journal.            
    """
    def __init__(self, parent=None, module=None, textEditWidget=None):
        """Paramètres:
               parent (obj) : Instance de QTreeWidget à laquelle intégrer (en tant qu'item) l'objet en cours.
               module (obj) : Module de vérification (fichier .py).
               textEditWidget (obj) : Instance de QTextEditWidget (correspondant au journal).
                    
           Retours :
               None.
        """
        QtWidgets.QTreeWidgetItem.__init__(self, parent)
        self.mainFrame = QtWidgets.QFrame()
        self.setupUi(self.mainFrame)

        self.moduleItem = module
        self.logArea = textEditWidget
        self.standardFuncNamesList = ["check", "select", "fix"]
        self.greenColor = QtGui.QColor(0,255,0,255)
        self.orangeColor = QtGui.QColor(255,165,0,255)
        self.redColor = QtGui.QColor(255,0,0,255)
        self.standardGreyColor = QtGui.QColor(177,177,177,255)
        self.titleContentFont = QtGui.QFont("Helvetica", 13, QtGui.QFont.Bold)
        self.logNormalFont = QtGui.QFont("Carlito", 12.5)

        self.set_module(parent, module)
        

    def set_module(self, parent=None, module=None):
        """Fixe les caractéristiques principales de l'item-module.
        
           Paramètres :
               parent (obj) : Instance de QTreeWidget.
               module (obj) : Module de vérification (fichier .py).

           Retours :
               None.
        """
        funcNameList = []
        startColOfStandardFunc = 2
        font = QtGui.QFont()

        colNb = startColOfStandardFunc + len(self.standardFuncNamesList)

        # Met en forme item-module (hors boutons)
        self.treeWidget().setItemWidget(self, 0, self.mainFrame)
        self.chB_sanityCheck_module.setText("  " + module.NAME)
        self.chB_sanityCheck_module.setToolTip(module.TOOLTIPS)
        font.setPointSize(12.5)
        self.chB_sanityCheck_module.setFont(font)

        # Genere boutons
        for funcName, funcObj in inspect.getmembers(module,inspect.isfunction):
            
            if funcName.startswith("__"):
                continue
            elif funcName == "pprint":
                continue
 
            funcNameList.append(funcName)
            # Bouton vide pour alignement des boutons de fonctions standards
            for nbOfStandardFunc in range(len(self.standardFuncNamesList)):
                if not self.standardFuncNamesList[nbOfStandardFunc] in funcNameList:
                    self.create_module_button(columnPos=startColOfStandardFunc+nbOfStandardFunc, standard=False,
                                              empty=True)
            # Bouton fonction non standard
            if not funcName in self.standardFuncNamesList:
                self.create_module_button(parent=parent, module=module, actionName=funcName, actionObj=funcObj, 
                                          columnPos=colNb, standard=False, text=funcName.upper())
                colNb = colNb + 1        
            # Boutons fonctions standards
            elif funcName == self.standardFuncNamesList[0]:
                self.create_module_button(parent=parent, module=module, actionName=funcName, actionObj=funcObj, 
                                          columnPos=2, standard=True, icon="magnifier_white.png")

            elif funcName == self.standardFuncNamesList[1]:
                self.create_module_button(parent=parent, module=module, actionName=funcName, actionObj=funcObj, 
                                          columnPos=3, standard=True, icon="selection.png")

            elif funcName == self.standardFuncNamesList[2]:
                self.create_module_button(parent=parent, module=module, actionName=funcName, actionObj=funcObj, 
                                          columnPos=4, standard=True, icon="repair_white.png")


    def create_module_button(self, parent=None, module=None, actionName=None, actionObj=None, columnPos=None, 
                             standard=None, icon=None, text="", empty=False):
        """Crée les boutons (vides, d'erreur, de fonctions standards ou non-standards) intégrés aux items-modules.

           Paramètres :
               parent (obj) : Instance de QTreeWidget.
               module (obj) : Module de vérification (fichier .py).
               actionName (str) : Nom de la fonction pour laquelle créer un bouton.
               actionObj (obj) : Fonction pour laquelle créer un bouton.
               columnPos (int) : Position (numéro de colonne) du bouton à créer dans le gridLayout du QTreeWidgetItem
                                 en cours.
               standard (bool) : Indication que le bouton à créer correspond à (i) une fonction standard ou bouton 
                                 d'erreur (True) ou (ii) à une fonction non standard ou bouton vide (False).
               icon (str) : Nom du fichier de l'icône à afficher sur le bouton à créer.
               text (str) : Texte à afficher sur le bouton à créer.
               empty (bool) : Indication que le bouton à créer est un bouton vide ou d'erreur (True) ou non (False).

           Retours :
               moduleButton (obj) : Instance de QPushButton.
        """
        moduleButton = QtWidgets.QPushButton(self.mainFrame)
        
        moduleButton.setAutoDefault(False)
        moduleButton.setDefault(False)
        moduleButton.setFlat(True)
        moduleButton.setText(text)
        if standard == True:
            moduleButton.setIcon(QtGui.QIcon(os.path.dirname(__file__) + "/supporting_files/icons/check/" + icon))
            moduleButton.setIconSize(QtCore.QSize(20,20))
        if empty == False:
            moduleButton.setMinimumSize(QtCore.QSize(0, 0))
            moduleButton.setMaximumSize(QtCore.QSize(30, 30))
            moduleButton.setToolTip(actionName.title())
            moduleButton.clicked.connect(lambda: self.generateAction(parent, module, actionName, actionObj))
        
        self.gridLayout.addWidget(moduleButton, 0, columnPos, 1, 1)

        return moduleButton


    def generateAction(self, parent=None, module=None, actionName=None, actionObj=None, goCheck=False):
        """Instancie la fonction du module, génère un éventuel message d'avertissement et le contenu du journal.
                
        Paramètres :
            parent (obj) : Instance de QTreeWidget.
            module (obj) : Module de vérification (fichier .py).
            actionName (str) : Nom de la fonction du module concernée.
            actionObj (obj) : Fonction du module concernée.
            goCheck (bool) : Interrupteur évitant de multiples instances de la fenêtre "pop-up" d'avertissement 
                             lorsqu'un "go check" est lancé sur plusieurs modules.

        Retours :
            None.
        """
        isSuccess = ""
        titleContent = []
        textContent = []
        flag = False
        nbOfDashes = 1
        fm = QtGui.QFontMetrics(self.titleContentFont)
        result = actionObj()

        # Genere message alerte pop-up quand rien n est selectionne
        if result == (True, False):
            if goCheck == False:
                self.popup_dialog()
                return
            else:
                return

        # Calcul nombre tirets dans titre journal
        titleWidthInPixels = fm.width(module.NAME.title() + ": " + actionName.title() + "\t" + 
                                      str(datetime.datetime.now()).partition('.')[0])
        for pixel in range(titleWidthInPixels):
            if flag == False and titleWidthInPixels < fm.width("-" * pixel):
                nbOfDashes = pixel
                flag = True

        # Ecriture titre journal
        self.logArea.setCurrentFont(self.titleContentFont)
        self.append_colorized_text(self.standardGreyColor, [module.NAME.title(), ": ", actionName.title(), "\t", 
                                   str(datetime.datetime.now()).partition('.')[0], "\n", "-" * nbOfDashes, "\n"*2])

        # Ecriture contenu journal
        self.logArea.setCurrentFont(self.logNormalFont)
        if result == (True, True) and actionName != self.standardFuncNamesList[2]:
            self.append_colorized_text(self.standardGreyColor, [actionName.upper(), " SUCCESSFULLY COMPLETED"])
            isSuccess = "noPb"
            if actionName == self.standardFuncNamesList[0] or actionName == self.standardFuncNamesList[1]:
                self.append_colorized_text(self.greenColor, ["\n"*2, "--> NO PROBLEM WAS IDENTIFIED", "\n"*3])
            else:
                self.logArea.append("\n"*3)

        elif result == (True,True) and actionName == self.standardFuncNamesList[2]:
            self.append_colorized_text(self.greenColor, [actionName.upper(), " SUCCESSFULLY COMPLETED"])
            self.append_colorized_text(self.standardGreyColor, ["\n"*2, "N.B.: Changes were made to the current scene ",
                                       "only if problems were identified by the program first.", "\n"*3])
            isSuccess = "noPb"

        elif result[0] == True and isinstance(result[1], (dict, list)) and not result[1] == {} and not result[1] == []:
            self.append_colorized_text(self.standardGreyColor, [actionName.upper(), 
                                       " SUCCESSFULLY COMPLETED ON THE SELECTED MESH(ES)"])
            self.append_colorized_text(self.orangeColor, ["\n"*2, "--> PROBLEMS WERE IDENTIFIED AT THESE LOCATIONS: ", 
                                       "\n"*2, json.dumps(result[1], sort_keys=True, indent=4), "\n"*3])
            isSuccess = "pb"        

        elif result[0] == False:
            self.append_colorized_text(self.redColor, [actionName.upper(), " FAILED:", "\n"*2, "RETURN: ", str(result), 
                                       "\n"*2, "--> PLEASE CONTACT THE DEV DEPARTMENT", "\n"*3])
            isSuccess = "crashed"

        else:
            self.append_colorized_text(self.redColor, ["ERROR: UNEXPECTED RETURN", "\n"*2, "RETURN: ", str(result), 
                                       "\n"*2, "--> PLEASE CONTACT THE DEV DEPARTMENT", "\n"*3])     
            isSuccess = "crashed"

        if isSuccess == "noPb":
            self.chB_sanityCheck_module.setStyleSheet("QCheckBox {color: #00ff00; font-weight: bold}")
        
        elif isSuccess == "pb":
            self.chB_sanityCheck_module.setStyleSheet("QCheckBox {color: orange; font-weight: bold}")
        
        else:
            self.create_module_button(columnPos=1, icon="warning.png", text=" ERROR", standard=True, 
                                      empty=True).setStyleSheet("QPushButton {color: red; font-weight: bold}")
            self.chB_sanityCheck_module.setStyleSheet("QCheckBox {color: red; font-weight: bold}")


    def popup_dialog(self):
        """Génère une fenêtre d'avertissement au-dessus de l'interface.
 
           Retours :
               None.
        """
        # Parentage de msgBox a logArea pour heriter de sa stylesheet
        msgBox = QtWidgets.QMessageBox(self.logArea)
        msgBox.setIcon(QtWidgets.QMessageBox.Warning)
        msgBox.setWindowTitle("Sanity Check: WARNING")
        msgBox.setText("Please select one or several meshes first! ")
        msgBox.setFont(self.logNormalFont)
        msgBox.addButton(QtWidgets.QMessageBox.Ok)
        # Pour etre sur que msgBox apparaisse toujours au-dessus de tout autre widget
        msgBox.raise_()
        
        msgBox.exec_()


    def append_colorized_text(self, color=None, text=None):
        """Ajoute du texte coloré au journal.
                
           Paramètres :
               color (obj) : Instance de QColor.
               text (list(str)) : Texte (sous forme d'une liste de chaînes de caractères) à colorer et à ajouter au 
                                  journal.

           Retours :
               None.
        """
        self.logArea.setTextColor(color)
        self.logArea.append("".join(text))
        self.logArea.setTextColor(self.standardGreyColor)



class ViewInterface(QtWidgets.QMainWindow, main_ui_py_file.Ui_MainWindow):
    """Instancie la fenêtre d'interface utilisateur de l'outil.

    Attributs :
        itemsList (list(obj)) : Liste des instances de MyModuleItem générées comme autant d'items du QTreeWidget intégré 
                                à l'interface utilisateur de l'outil.
        headersList (list(obj)) : Liste d'instances de MyHeaderItem créées au lancement de l'outil.
        _path (str) : Chemin relatif pointant vers le dossier dans lequel se trouvent tous les modules de vérification.
        menu (obj) : Instance de QMenu (correspondant au menu contextuel accessible dans le journal).
        clearAction (obj) : Instance de QAction figurant dans le menu contextuel accessible dans le journal et dont 
                            l'exécution par l'utilisateur entraîne la suppression du contenu du journal.
        copyAction (obj) : Instance de QAction figurant dans le menu contextuel accessible dans le journal et dont 
                           l'exécution par l'utilisateur entraîne la copie du contenu sélectionné du journal.        
        settings_path (str) : Chemin relatif pointant vers le fichier .ini contenant les préférences utilisateur de 
                              taille et de position de la fenêtre de l'outil et de son QSplitter.
    """
    def __init__(self, parent=None):
        """Paramètres:
               parent (obj) : Instance de QMainWindow à laquelle intégrer l'outil.
            
           Retours :
               None.
        """
        QtWidgets.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        self.itemsList = []
        self.headersList = []
        self._path = os.path.join(os.path.dirname(__file__), "checkers")
        self.menu = QtWidgets.QMenu()
        self.clearAction = self.menu.addAction("Clear")
        self.copyAction = self.menu.addAction("Copy")        
        
        self.set_sanitycheck()
        self.get_modules(self.trW_sanityCheck,self._path)
        self.memorize_size_position()

        self.trW_sanityCheck.itemExpanded.connect(self.frame_sunken)
        self.trW_sanityCheck.itemCollapsed.connect(self.frame_raised)
        self.puB_sanityCheck_goCheck.clicked.connect(self.go_check)
        self.teE_sanityCheck.customContextMenuRequested.connect(self.addMenuActions)
        self.liE_sanityCheck.textChanged.connect(self.search_module)
        self.act_sanityCheck_reset.triggered.connect(self.reset_sanitycheck)  


    def set_sanitycheck(self):
        """Fixe quelques caractéristiques générales de l'outil.

           Retours :
               None.
        """
        self.spl_sanityCheck.setSizes([0,0])
        self.teE_sanityCheck.setAlignment(QtCore.Qt.AlignRight)
        self.trW_sanityCheck.header().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        self.teE_sanityCheck.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)


    def get_modules(self, parent=None, path=None):
        """Scanne l'arborescence et instancie en conséquence MyHeaderItem/MyModuleItem par dossier/fichier.
                
           Paramètres :
               parent (obj) : Instance de QTreeWidget.
               path (str) : Chemin pointant vers le dossier dans lequel se trouvent tous les modules de vérification.

           Retours :
               None.
        """
        for file in sorted(sorted(os.listdir(path)), key=lambda x: x if not os.path.isdir(os.path.join(path, x)) 
        else False):
            files_path = os.path.join(path, file)
            if os.path.isdir(files_path):
                header = MyHeaderItem(parent, file.replace("_", " "))
                self.get_modules(header,files_path)
                self.headersList.append(header)
                continue

            elif file.startswith("__"):
                continue
            elif not file.endswith(".py"):
                continue

            mod_path = "sanity_check" + path.replace(os.path.dirname(__file__), "").replace("/", ".")

            try:
                curModule = reload(importlib.import_module("." + file.partition(".")[0], mod_path))
                curItem = MyModuleItem(parent, curModule, self.teE_sanityCheck)
                self.itemsList.append(curItem)
            except:
                print "Module défaillant : ", file
                print "Erreur : ", sys.exc_info()[0]


    def memorize_size_position(self):
        """Mémorise les taille et position de la fenêtre et la position du QSplitter à la fermeture de l'outil.
                
           Retours :
               None.
        """        
        # Cree dossier "sanity_check" dans dossier utilisateur pour y placer fichier .ini contenant preferences 
        # utilisateur de taille et position de mainWindow et QSplitter
        if not os.path.exists(os.path.join(os.path.expanduser('~'), "Documents/sanity_check")):
            os.mkdir(os.path.join(os.path.expanduser('~'), "Documents/sanity_check"))
        self.settings_path = os.path.join(os.path.expanduser('~'), "Documents/sanity_check/sanity_check_settings.ini")
        # Restaure dernieres infos de taille et position de mainWindow et QSplitter au lancement de l outil
        if os.path.exists(self.settings_path):
            settings_obj = QtCore.QSettings(self.settings_path, QtCore.QSettings.IniFormat)
            self.restoreGeometry(settings_obj.value("windowGeometry"))
            self.spl_sanityCheck.restoreState(settings_obj.value("splitterSettings"))


    def frame_sunken(self, currentItem=None):
        """Applique l'effet de style "enfoncé" ("sunken") de l'item-titre ("header") courant.

           Paramètres :
               currentItem (obj) : Item-titre ("header") courant.

           Retours :
               None.
        """
        currentItem.apply_sunken_frame()


    def frame_raised(self, currentItem=None):   
        """Applique l'effet de style "surélevé" ("raised") de l'item-titre ("header") courant.

           Paramètres :
               currentItem (obj) : Item-titre ("header") courant.

           Retours :
               None.
        """
        currentItem.apply_raised_frame()


    def go_check(self):
        """Exécute la fonction "check" de tous les modules sélectionnés.

           Retours :
               None.
        """
        flag = False

        for item in self.itemsList:
            if item.chB_sanityCheck_module.checkState() != QtCore.Qt.CheckState.Checked:
                continue

            for funcName, funcObj in inspect.getmembers(item.moduleItem,inspect.isfunction):
                if funcName != item.standardFuncNamesList[0]:
                    continue

                if funcObj() == (True,False) and flag == False:
                    item.generateAction(parent=self.trW_sanityCheck, module=item.moduleItem, 
                                        actionName=funcName, actionObj=funcObj, goCheck=False)
                    flag = True
                else:
                    item.generateAction(parent=self.trW_sanityCheck, module=item.moduleItem, 
                                        actionName=funcName, actionObj=funcObj, goCheck=True)


    def addMenuActions(self, position=None):
        """Crée un menu contextuel personnalisé dans le journal.

           Paramètres :
               position (obj) : Position (x,y) de la souris lors du clic droit (instance de QtCore.QPoint). 

           Retours :
               None.
        """
        action = self.menu.exec_(self.teE_sanityCheck.mapToGlobal(position))

        if action == self.clearAction:
            self.teE_sanityCheck.clear()
            self.teE_sanityCheck.setAlignment(QtCore.Qt.AlignRight)
        elif action == self.copyAction:
            self.teE_sanityCheck.copy()


    def search_module(self, text=None):
        """Régit le comportement de la barre de recherche.

           Paramètres :
               text (str) : Texte saisi dans la barre de recherche. 

           Retours :
               None.
        """
        headersToStayExpanded = []
        # Si pas de texte tout est visible
        if text == "":
            for item in self.itemsList:
                item.setHidden(False)
            for header in self.headersList:
                header.setHidden(False)
                self.trW_sanityCheck.collapseItem(header)
        # Si texte
        else:
            # Pour chaque module
            for item in self.itemsList:
                # Si module pertinent
                if text.lower() in item.moduleItem.NAME.lower():
                    # Force visibilite de l item-module (potentiellement cache depuis saisie anterieure)
                    item.setHidden(False)
                    # Developpe ("expand") et force visibilite des items-titres ("headers") directs et indirects de 
                    # l item-module
                    for treeLevel in range(self.trW_sanityCheck.topLevelItemCount()):
                        if isinstance(item.parent(), MyHeaderItem):                      
                            self.trW_sanityCheck.setItemExpanded(item.parent(),1)
                            item = item.parent()
                            item.setHidden(False)
                            headersToStayExpanded.append(item)
                        else:
                            break
                    # Optionnel : Cache tout item-titre ("header") non pertinent                            
                    for headerToBeHidden in self.headersList:
                        if not str(headerToBeHidden) in str(headersToStayExpanded):
                            headerToBeHidden.setHidden(True)
                # Si module non pertinent, cache item-module
                else:
                    item.setHidden(True)


    def closeEvent(self, event=None):
        """Réimplémente le gestionnaire d'évènement closeEvent().

           Paramètres :
               event (obj) : Objet QCloseEvent envoyé à la présente méthode lorsque PySide reçoit du système une 
                             demande de fermeture de fenêtre pour un widget situé au niveau le plus élevé.

           Retours :
               None.
        """
        # Enregistre dans fichier .ini infos de taille et position de mainWindow et QSplitter a la fermeture de l outil
        settings_obj = QtCore.QSettings(self.settings_path, QtCore.QSettings.IniFormat)
        settings_obj.setValue("windowGeometry", self.saveGeometry())
        settings_obj.setValue("splitterSettings", self.spl_sanityCheck.saveState())

 
    def reset_sanitycheck(self):
        """Réinitialise l'outil (suppression du contenu du journal et rechargement des modules).

           Retours :
               None.
        """        
        self.teE_sanityCheck.clear()
        self.trW_sanityCheck.clear()
        self.get_modules(self.trW_sanityCheck,self._path)