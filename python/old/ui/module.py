# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/users_roaming/tperilhou/dev/studio3d_dev/pipeline/maya/python/sanity_check/ui/module.ui'
#
# Created: Fri Jul 19 10:53:24 2019
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_module_frame(object):
    def setupUi(self, module_frame):
        module_frame.setObjectName("module_frame")
        module_frame.resize(789, 38)
        module_frame.setStyleSheet("QFrame\n"
"{ \n"
"   background-color: #242424;\n"
"   margin-left: 0px;\n"
"   border-top-left-radius: 4px;\n"
"   border-bottom-left-radius: 4px;\n"
"   border-bottom: 0.5px solid orange;\n"
"   border-left: 0.5px solid orange;\n"
"}\n"
"\n"
"QCheckBox\n"
"{ \n"
"   background-color: #242424;\n"
"   border-radius: 2px;\n"
"}\n"
"\n"
"QCheckBox::indicator{\n"
"    color: #b1b1b1;\n"
"    background-color: #323232;\n"
"    border: 1px solid #b1b1b1;\n"
"    width: 9px;\n"
"    height: 9px;\n"
"    border-radius: 2px;\n"
"}\n"
"\n"
"QCheckBox::indicator:hover\n"
"{    \n"
"    border: 1px solid #ffaa00;\n"
"    border-radius: 2px;\n"
"}\n"
"\n"
"QCheckBox::indicator:checked\n"
"{\n"
"   background-color: #d7801a;\n"
"   border: 1px solid #ffaa00;\n"
"   border-radius: 2px;\n"
"}\n"
"\n"
"QCheckBox::indicator:disabled\n"
"{\n"
"    border: 1px solid #444;\n"
"    border-radius: 2px;\n"
"}\n"
"\n"
"QPushButton:hover\n"
"{\n"
"    border: 0px solid QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"}\n"
"\n"
"QPushButton\n"
"{\n"
"    color: #b1b1b1;\n"
"    background-color: #242424;\n"
"    border-width: 1px;\n"
"    border-color: #1e1e1e;\n"
"    border-style: flat;\n"
"    border-radius: 6;\n"
"    padding: 3px;\n"
"    font-size: 12px;\n"
"    padding-left: 5px;\n"
"    padding-right: 5px;\n"
"}\n"
"\n"
"QPushButton:pressed\n"
"{\n"
"    background-color: #242424;\n"
"}")
        module_frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        module_frame.setFrameShadow(QtWidgets.QFrame.Plain)
        module_frame.setLineWidth(5)
        self.gridLayout = QtWidgets.QGridLayout(module_frame)
        self.gridLayout.setObjectName("gridLayout")
        spacerItem = QtWidgets.QSpacerItem(551, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 1, 1, 1)
        self.chB_sanityCheck_module = QtWidgets.QCheckBox(module_frame)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setWeight(75)
        font.setBold(True)
        self.chB_sanityCheck_module.setFont(font)
        self.chB_sanityCheck_module.setStyleSheet("")
        self.chB_sanityCheck_module.setObjectName("chB_sanityCheck_module")
        self.gridLayout.addWidget(self.chB_sanityCheck_module, 0, 0, 1, 1)

        self.retranslateUi(module_frame)
        QtCore.QMetaObject.connectSlotsByName(module_frame)

    def retranslateUi(self, module_frame):
        module_frame.setWindowTitle(QtWidgets.QApplication.translate("module_frame", "Frame", None, -1))
        self.chB_sanityCheck_module.setText(QtWidgets.QApplication.translate("module_frame", "item_name", None, -1))

