# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/users_roaming/tperilhou/dev/studio3d_dev/pipeline/maya/python/sanity_check/ui/header.ui'
#
# Created: Fri Jul 19 10:53:24 2019
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_header_frame(object):
    def setupUi(self, header_frame):
        header_frame.setObjectName("header_frame")
        header_frame.resize(382, 39)
        header_frame.setStyleSheet("QFrame\n"
"{ \n"
"   background-color: #242424;\n"
"}\n"
"\n"
"QPushButton:checked\n"
"{\n"
"    border: 2px solid QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #2d2d2d, stop: 0.1 #2b2b2b, stop: 0.5 #292929, stop: 0.9 #282828, stop: 1 #252525);\n"
"}")
        header_frame.setFrameShape(QtWidgets.QFrame.WinPanel)
        header_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        header_frame.setLineWidth(5)
        self.gridLayout = QtWidgets.QGridLayout(header_frame)
        self.gridLayout.setObjectName("gridLayout")
        spacerItem = QtWidgets.QSpacerItem(228, 18, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 2, 1, 1)
        self.puB_sanityCheck_header = QtWidgets.QPushButton(header_frame)
        self.puB_sanityCheck_header.setMaximumSize(QtCore.QSize(20, 20))
        self.puB_sanityCheck_header.setText("")
        self.puB_sanityCheck_header.setCheckable(True)
        self.puB_sanityCheck_header.setObjectName("puB_sanityCheck_header")
        self.gridLayout.addWidget(self.puB_sanityCheck_header, 0, 0, 1, 1)
        self.lab_sanityCheck_header = QtWidgets.QLabel(header_frame)
        font = QtGui.QFont()
        font.setFamily("Sans Serif")
        font.setPointSize(12)
        font.setWeight(75)
        font.setBold(True)
        self.lab_sanityCheck_header.setFont(font)
        self.lab_sanityCheck_header.setObjectName("lab_sanityCheck_header")
        self.gridLayout.addWidget(self.lab_sanityCheck_header, 0, 1, 1, 1)

        self.retranslateUi(header_frame)
        QtCore.QMetaObject.connectSlotsByName(header_frame)

    def retranslateUi(self, header_frame):
        header_frame.setWindowTitle(QtWidgets.QApplication.translate("header_frame", "Frame", None, -1))
        self.lab_sanityCheck_header.setText(QtWidgets.QApplication.translate("header_frame", " header_name", None, -1))

