# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/users_roaming/tperilhou/dev/studio3d_dev/pipeline/maya/python/sanity_check/ui/sanity_check_main_ui.ui'
#
# Created: Fri Jul 19 10:53:24 2019
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(503, 684)
        font = QtGui.QFont()
        font.setPointSize(9)
        MainWindow.setFont(font)
        MainWindow.setStyleSheet("QTreeWidget\n"
"{\n"
"    background-color: #242424;\n"
"}\n"
"\n"
"QToolTip\n"
"{\n"
"     border: 1px solid black;\n"
"     background-color: #ffa02f;\n"
"     padding: 1px;\n"
"}\n"
"\n"
"QWidget\n"
"{\n"
"    color: #b1b1b1;\n"
"    background-color: #323232;\n"
"}\n"
"\n"
"QWidget:item:hover\n"
"{\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #ca0619);\n"
"    color: #000000;\n"
"}\n"
"\n"
"QWidget:item:selected\n"
"{\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"}\n"
"\n"
"QMenu\n"
"{\n"
"    border: 1px solid #000;\n"
"}\n"
"\n"
"QMenu::item\n"
"{\n"
"    padding: 2px 20px 2px 20px;\n"
"}\n"
"\n"
"QMenu::item:selected\n"
"{\n"
"    color: #000000;\n"
"}\n"
"\n"
"QWidget:disabled\n"
"{\n"
"    color: #404040;\n"
"    background-color: #323232;\n"
"}\n"
"\n"
"QAbstractItemView\n"
"{\n"
"    background-color: #242424;\n"
"}\n"
"\n"
"QWidget:focus\n"
"{\n"
"    /*border: 2px solid QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);*/\n"
"}\n"
"\n"
"QPushButton:hover\n"
"{\n"
"    border: 2px solid QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"}\n"
"\n"
"QPushButton\n"
"{\n"
"    color: #b1b1b1;\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"    border-width: 1px;\n"
"    border-color: #1e1e1e;\n"
"    border-style: solid;\n"
"    border-radius: 6;\n"
"    padding: 3px;\n"
"    font-size: 12px;\n"
"    padding-left: 5px;\n"
"    padding-right: 5px;\n"
"    font-weight:bold;\n"
"}\n"
"\n"
"QPushButton:pressed\n"
"{\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #2d2d2d, stop: 0.1 #2b2b2b, stop: 0.5 #292929, stop: 0.9 #282828, stop: 1 #252525);\n"
"}\n"
"\n"
"\n"
"QScrollBar::handle:horizontal\n"
"{\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #ffa02f, stop: 0.5 #d7801a, stop: 1 #ffa02f);\n"
"      min-height: 20px;\n"
"      border-radius: 2px;\n"
"}\n"
"\n"
"QScrollBar::add-line:horizontal {\n"
"      border: 1px solid #1b1b19;\n"
"      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"      width: 14px;\n"
"      subcontrol-position: right;\n"
"      subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:horizontal {\n"
"      border: 1px solid #1b1b19;\n"
"      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"      width: 14px;\n"
"     subcontrol-position: left;\n"
"     subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::right-arrow:horizontal, QScrollBar::left-arrow:horizontal\n"
"{\n"
"      border: 1px solid black;\n"
"      width: 1px;\n"
"      height: 1px;\n"
"      background: white;\n"
"}\n"
"\n"
"QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal\n"
"{\n"
"      background: none;\n"
"}\n"
"\n"
"QScrollBar:vertical\n"
"{\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0.0 #121212, stop: 0.2 #282828, stop: 1 #484848);\n"
"      width: 11px;\n"
"      margin: 16px 0 16px 0;\n"
"      border: 1px solid #222222;\n"
"}\n"
"\n"
"QScrollBar::handle:vertical\n"
"{\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 0.5 #d7801a, stop: 1 #ffa02f);\n"
"      min-height: 20px;\n"
"      border-radius: 2px;\n"
"}\n"
"\n"
"QScrollBar::add-line:vertical\n"
"{\n"
"      border: 1px solid #1b1b19;\n"
"      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"      height: 14px;\n"
"      subcontrol-position: bottom;\n"
"      subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:vertical\n"
"{\n"
"      border: 1px solid #1b1b19;\n"
"      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #d7801a, stop: 1 #ffa02f);\n"
"      height: 14px;\n"
"      subcontrol-position: top;\n"
"      subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::up-arrow:vertical, QScrollBar::down-arrow:vertical\n"
"{\n"
"      border: 1px solid black;\n"
"      width: 1px;\n"
"      height: 1px;\n"
"      background: white;\n"
"}\n"
"\n"
"\n"
"QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical\n"
"{\n"
"      background: none;\n"
"}\n"
"\n"
"QTextEdit\n"
"{\n"
"    background-color: #242424;\n"
"}\n"
"\n"
"QHeaderView::section\n"
"{\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #616161, stop: 0.5 #505050, stop: 0.6 #434343, stop:1 #656565);\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #6c6c6c;\n"
"}\n"
"\n"
"QCheckBox:disabled\n"
"{\n"
"color: #414141;\n"
"}\n"
"\n"
"QMainWindow::separator\n"
"{\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #161616, stop: 0.5 #151515, stop: 0.6 #212121, stop:1 #343434);\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #4c4c4c;\n"
"    spacing: 3px; /* spacing between items in the tool bar */\n"
"}\n"
"\n"
"QMainWindow::separator:hover\n"
"{\n"
"\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #d7801a, stop:0.5 #b56c17 stop:1 #ffa02f);\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #6c6c6c;\n"
"    spacing: 3px; /* spacing between items in the tool bar */\n"
"}\n"
"\n"
"QMenu::separator\n"
"{\n"
"    height: 2px;\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #161616, stop: 0.5 #151515, stop: 0.6 #212121, stop:1 #343434);\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    margin-left: 10px;\n"
"    margin-right: 5px;\n"
"}\n"
"\n"
"QProgressBar\n"
"{\n"
"    border: 2px solid grey;\n"
"    border-radius: 5px;\n"
"    text-align: center;\n"
"}\n"
"\n"
"QProgressBar::chunk\n"
"{\n"
"    background-color: #d7801a;\n"
"    width: 2.15px;\n"
"    margin: 0.5px;\n"
"}\n"
"\n"
"QLineEdit\n"
"{\n"
"   border: 1px solid orange;\n"
"}")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.puB_sanityCheck_goCheck = QtWidgets.QPushButton(self.centralwidget)
        self.puB_sanityCheck_goCheck.setMinimumSize(QtCore.QSize(0, 35))
        font = QtGui.QFont()
        font.setPointSize(-1)
        font.setWeight(75)
        font.setBold(True)
        self.puB_sanityCheck_goCheck.setFont(font)
        self.puB_sanityCheck_goCheck.setStyleSheet("")
        self.puB_sanityCheck_goCheck.setObjectName("puB_sanityCheck_goCheck")
        self.gridLayout.addWidget(self.puB_sanityCheck_goCheck, 2, 0, 1, 1)
        self.liE_sanityCheck = QtWidgets.QLineEdit(self.centralwidget)
        self.liE_sanityCheck.setMinimumSize(QtCore.QSize(0, 25))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setWeight(50)
        font.setItalic(True)
        font.setBold(False)
        self.liE_sanityCheck.setFont(font)
        self.liE_sanityCheck.setStyleSheet("")
        self.liE_sanityCheck.setText("")
        self.liE_sanityCheck.setClearButtonEnabled(True)
        self.liE_sanityCheck.setObjectName("liE_sanityCheck")
        self.gridLayout.addWidget(self.liE_sanityCheck, 0, 0, 1, 1)
        self.spl_sanityCheck = QtWidgets.QSplitter(self.centralwidget)
        self.spl_sanityCheck.setStyleSheet("")
        self.spl_sanityCheck.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.spl_sanityCheck.setFrameShadow(QtWidgets.QFrame.Plain)
        self.spl_sanityCheck.setOrientation(QtCore.Qt.Horizontal)
        self.spl_sanityCheck.setHandleWidth(10)
        self.spl_sanityCheck.setObjectName("spl_sanityCheck")
        self.trW_sanityCheck = QtWidgets.QTreeWidget(self.spl_sanityCheck)
        self.trW_sanityCheck.setAutoFillBackground(False)
        self.trW_sanityCheck.setStyleSheet("")
        self.trW_sanityCheck.setTabKeyNavigation(True)
        self.trW_sanityCheck.setAlternatingRowColors(False)
        self.trW_sanityCheck.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        self.trW_sanityCheck.setHeaderHidden(True)
        self.trW_sanityCheck.setColumnCount(1)
        self.trW_sanityCheck.setObjectName("trW_sanityCheck")
        self.trW_sanityCheck.headerItem().setText(0, "1")
        self.trW_sanityCheck.header().setVisible(False)
        self.trW_sanityCheck.header().setStretchLastSection(False)
        self.teE_sanityCheck = QtWidgets.QTextEdit(self.spl_sanityCheck)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.teE_sanityCheck.setFont(font)
        self.teE_sanityCheck.setReadOnly(True)
        self.teE_sanityCheck.setObjectName("teE_sanityCheck")
        self.gridLayout.addWidget(self.spl_sanityCheck, 1, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.staB_sanityCheck = QtWidgets.QStatusBar(MainWindow)
        self.staB_sanityCheck.setObjectName("staB_sanityCheck")
        MainWindow.setStatusBar(self.staB_sanityCheck)
        self.menB_sanityCheck = QtWidgets.QMenuBar(MainWindow)
        self.menB_sanityCheck.setGeometry(QtCore.QRect(0, 0, 503, 20))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.menB_sanityCheck.setFont(font)
        self.menB_sanityCheck.setObjectName("menB_sanityCheck")
        self.menF_sanityCheck = QtWidgets.QMenu(self.menB_sanityCheck)
        self.menF_sanityCheck.setObjectName("menF_sanityCheck")
        MainWindow.setMenuBar(self.menB_sanityCheck)
        self.act_sanityCheck_reset = QtWidgets.QAction(MainWindow)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.act_sanityCheck_reset.setFont(font)
        self.act_sanityCheck_reset.setObjectName("act_sanityCheck_reset")
        self.actionQuit = QtWidgets.QAction(MainWindow)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.actionQuit.setFont(font)
        self.actionQuit.setObjectName("actionQuit")
        self.act_sanityCheck_quit = QtWidgets.QAction(MainWindow)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.act_sanityCheck_quit.setFont(font)
        self.act_sanityCheck_quit.setObjectName("act_sanityCheck_quit")
        self.menF_sanityCheck.addAction(self.act_sanityCheck_reset)
        self.menF_sanityCheck.addSeparator()
        self.menB_sanityCheck.addAction(self.menF_sanityCheck.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtWidgets.QApplication.translate("MainWindow", "SANITY CHECK", None, -1))
        self.puB_sanityCheck_goCheck.setText(QtWidgets.QApplication.translate("MainWindow", "GO CHECK", None, -1))
        self.liE_sanityCheck.setPlaceholderText(QtWidgets.QApplication.translate("MainWindow", "Search for a checker here", None, -1))
        self.menF_sanityCheck.setTitle(QtWidgets.QApplication.translate("MainWindow", "File", None, -1))
        self.act_sanityCheck_reset.setText(QtWidgets.QApplication.translate("MainWindow", "Reset", None, -1))
        self.actionQuit.setText(QtWidgets.QApplication.translate("MainWindow", "Quit", None, -1))
        self.act_sanityCheck_quit.setText(QtWidgets.QApplication.translate("MainWindow", "Quit", None, -1))

