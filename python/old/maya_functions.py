# -*- coding: utf-8 -*-
import maya.cmds as cmds

from logger   import set_logger

def getSelectedMesh(mtype = 'mesh'):
    """ Returns Meshes from selection. """
    cmds.selectMode( object=True )
    selection = cmds.ls(selection = True, long = True, noIntermediate = True, o=True, type = 'mesh', dag = True) or []
    # cmds.ls(selection = True, long = True, noIntermediate = True) or []

    # if not cmds.listRelatives(selection, p = True, type = 'transform'): 
    #     selection = cmds.listRelatives(selection, fullPath = True, allDescendents = True, type = mtype)

    return selection

def getAllMesh(mtype = 'mesh'):
    selection = cmds.ls( type=mtype, long=True, noIntermediate=True)
    return selection