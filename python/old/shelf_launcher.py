import sys
from pprint import pprint

path = '/prod/studio3d_dev/partage'

if not path in sys.path:
    sys.path.insert(0, path )
    pprint( sys.path )

from sanity_check import launcher

reload(launcher)

launcher.run()