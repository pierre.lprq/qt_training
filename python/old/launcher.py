# -*- coding: utf-8 -*-
# Source : https://help.autodesk.com/view/MAYAUL/2017/ENU/?guid=__files_GUID_3F96AF53_A47E_4351_A86A_396E7BFD6665_htm

# Maya Imports
from maya               import OpenMayaUI
import maya.cmds        as cmds

# Pyside Imports
from PySide2            import QtCore, QtGui, QtWidgets
from shiboken2          import wrapInstance

# Interface Import
from sanity_check       import view

# Interface UI to PY
# import pyside2uic, os
# pyside2uic.compileUiDir(os.path.dirname(__file__) + '/ui')

# Reloads
reload(view)

def run():

    global PROCVIEW

    ptr = OpenMayaUI.MQtUtil.mainWindow()
    main_window = wrapInstance(long(ptr), QtWidgets.QMainWindow)

    # Close the window if already opened
    if 'PROCVIEW' in globals():
        PROCVIEW.close()

    # Create an instance of ViewInterface from view.py
    PROCVIEW = view.ViewInterface(main_window)

    # Display
    PROCVIEW.show()